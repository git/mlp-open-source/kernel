/*
 * LCD Driver Example
 *
 * Copyright 2013 Texas Instruments
 *
 * Author: Milo Kim <milo.kim@ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/delay.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/regulator/consumer.h>
#include <linux/platform_device.h>
#include <linux/slab.h>

#define LCD_BOOST_VOUT		6000000
#define LCD_BIAS_VOUT		5500000

enum lcd_ldo_id {
	VBOOST,
	VPOS,
	VNEG,
	NUM_REGULATORS,
};

const char *supplies[] = { "lcd_boost", "lcd_vpos", "lcd_vneg", };

struct lcd_ctrl {
	struct regulator *r[NUM_REGULATORS];
	struct device *dev;
};

static int lm_lcd_enable(struct lcd_ctrl *ctl, enum lcd_ldo_id id, int enable)
{
	if (!ctl->r[id]) {
		dev_info(ctl->dev, "regulator is not ready\n");
		return 0;
	}

	if (enable)
		return regulator_enable(ctl->r[id]);
	else
		return regulator_disable(ctl->r[id]); 
}

static int lm_lcd_set_voltage(struct lcd_ctrl *ctl, enum lcd_ldo_id id, int uV)
{
	return regulator_set_voltage(ctl->r[id], uV, uV + 10000);
}

static int lm_lcd_probe(struct platform_device *pdev)
{
	struct lcd_ctrl *ctl;
	struct device *dev = &pdev->dev;
	struct regulator *r;
	int i;

	ctl = devm_kzalloc(dev, sizeof(*ctl), GFP_KERNEL);
	if (!ctl)
		return -ENOMEM;

	ctl->dev = dev;

	for (i = 0; i < NUM_REGULATORS; i++) {
		r = devm_regulator_get(dev, supplies[i]);
		if (IS_ERR(r))
			continue;

		ctl->r[i] = r;
	}

	platform_set_drvdata(pdev, ctl);

	lm_lcd_set_voltage(ctl, VBOOST, LCD_BOOST_VOUT);
	lm_lcd_set_voltage(ctl, VPOS, LCD_BIAS_VOUT);
	lm_lcd_set_voltage(ctl, VNEG, LCD_BIAS_VOUT);

	lm_lcd_enable(ctl, VPOS, 1);
	lm_lcd_enable(ctl, VNEG, 1);

	return 0;
}

static int lm_lcd_remove(struct platform_device *pdev)
{
	struct lcd_ctrl *ctl = platform_get_drvdata(pdev);

	lm_lcd_enable(ctl, VPOS, 0);
	lm_lcd_enable(ctl, VNEG, 0);

	return 0;
}

static struct platform_driver lcd_ctrl_driver = {
	.probe = lm_lcd_probe,
	.remove = lm_lcd_remove,
	.driver = {
		.name = "lm-lcd-driver",
		.owner = THIS_MODULE,
	},
};
module_platform_driver(lcd_ctrl_driver);

MODULE_DESCRIPTION("LCD Controller Driver Example");
MODULE_AUTHOR("Milo Kim");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:lm-lcd-driver");
